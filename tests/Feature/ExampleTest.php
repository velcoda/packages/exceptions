<?php

namespace Velcoda\Exceptions\Exceptions\Tests\Feature;

use Velcoda\Exceptions\Exceptions\Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
