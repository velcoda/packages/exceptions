<?php

namespace Velcoda\Exceptions\Exceptions;

use Exception;
use Illuminate\Http\Response;
use Throwable;

class Base extends Exception // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 500;
    protected $status_message = 'INTERNAL_SERVER_ERROR';
    protected $trace;

    public function __construct($message = '', $code = null, Throwable $previous = null)
    {
        if ($code) {
            $this->status_code = $code;
        }
        if ($previous) {
            $this->trace = $previous->getTrace();
        } else {
            $this->trace = $this->getTrace();
        }
        parent::__construct($message, $this->status_code, $previous);
    }

    public function render($request, $exception = null)
    {
        $data = [
            'code' => $this->status_code,
            'message' => $this->status_message,
        ];
        if ($this->message) {
            $data['details'] = $this->message;
        }
        if (env('APP_DEBUG')) {
            $data['trace'] = $this->trace;
        }
        return new Response($data, $this->status_code);
    }
}
