<?php

namespace Velcoda\Exceptions\Exceptions;

class HTTP_NOT_FOUND extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 404;
    protected $status_message = 'HTTP_NOT_FOUND';
    protected $message = 'Resource not found.';
}
