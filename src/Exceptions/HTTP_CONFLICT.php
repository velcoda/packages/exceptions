<?php

namespace Velcoda\Exceptions\Exceptions;

class HTTP_CONFLICT extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 409;
    protected $status_message = 'HTTP_CONFLICT';
}
