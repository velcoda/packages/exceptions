<?php

namespace Velcoda\Exceptions\Exceptions;

class NotImplementedException extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 500;
    protected $status_message = 'NOT_IMPLEMENTED';
}
