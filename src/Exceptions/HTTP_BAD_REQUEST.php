<?php

namespace Velcoda\Exceptions\Exceptions;

class HTTP_BAD_REQUEST extends Base
{
    protected $status_code = 400;
    protected $status_message = 'HTTP_BAD_REQUEST';
}
