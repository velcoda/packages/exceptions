<?php

namespace Velcoda\Exceptions\Exceptions;

class HTTP_INTERNAL_SERVER extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 500;
    protected $status_message = 'HTTP_INTERNAL_SERVER';
}
