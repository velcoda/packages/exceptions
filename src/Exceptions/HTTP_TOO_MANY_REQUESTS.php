<?php

namespace Velcoda\Exceptions\Exceptions;

use Illuminate\Http\Response;

class HTTP_TOO_MANY_REQUESTS extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 429;
    protected $status_message = 'HTTP_TOO_MANY_REQUESTS';

    public function render($request, $exception = null)
    {
        $reasons = $exception?->validator?->getMessageBag()->getMessages();
        $data = [
            'code' => $this->status_code,
            'message' => $this->status_message,
        ];
        if ($reasons) {
            $data['reasons'] = $reasons;
        }
        if ($this->message) {
            $data['details'] = $this->message;
        }
        if (env('APP_DEBUG')) {
            $data['trace'] = $this->trace;
        }
        return new Response($data, $this->status_code);
    }
}
