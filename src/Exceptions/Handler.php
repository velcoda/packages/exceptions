<?php

namespace Velcoda\Exceptions\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\UnauthorizedException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        UnauthorizedException::class,
        ModelNotFoundException::class,
        HTTP_UNAUTHORIZED::class,
    ];
    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Exception $exception
     *
     * @return void
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
        if ($this->shouldntReport($exception)) {
            return;
        }
        if (extension_loaded('newrelic') && function_exists('newrelic_notice_error')) { // Ensure PHP agent is available
            newrelic_notice_error($exception->getMessage(), $exception);
        } else {
            if (in_array(env('APP_ENV'), ['staging', 'production'])) {
                // disable because newrelic ratelimit lets the App endpoints timeout.
                // throw new Exception('NewRelic not installed or loaded.');
            }
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request   $request
     * @param Throwable $exception
     *
     * @return Response
     * @throws Throwable
     */
    public function render($request, Throwable $exception)
    {
        // for laravel passport exception
        if ($exception instanceof AuthenticationException) {
            return (new \Velcoda\Exceptions\Exceptions\HTTP_UNAUTHORIZED)->render($request, $exception);
        }
        if ($exception instanceof ValidationException) {
            return (new \Velcoda\Exceptions\Exceptions\HTTP_UNPROCESSABLE_ENTITY)->render($request, $exception);
        }
        return parent::render($request, $exception);
    }
}
