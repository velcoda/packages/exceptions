<?php

namespace Velcoda\Exceptions\Exceptions;

class HTTP_FORBIDDEN extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 403;
    protected $status_message = 'HTTP_FORBIDDEN';
}
