<?php

namespace Velcoda\Exceptions\Exceptions;

class HTTP_UNAUTHORIZED extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 401;
    protected $status_message = 'HTTP_UNAUTHORIZED';
}
