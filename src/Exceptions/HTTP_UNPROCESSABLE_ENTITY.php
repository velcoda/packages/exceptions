<?php

namespace Velcoda\Exceptions\Exceptions;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class HTTP_UNPROCESSABLE_ENTITY extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 422;
    protected $status_message = 'HTTP_UNPROCESSABLE_ENTITY';

    public function render($request, $exception = null)
    {
        $reasons = $exception?->validator?->getMessageBag()->getMessages();
        $data = [
            'code' => $this->status_code,
            'message' => $this->status_message,
        ];
        if ($reasons) {
            $data['reasons'] = $reasons;
        }
        if ($this->message) {
            $data['details'] = $this->message;
        }
        if (env('APP_DEBUG')) {
            $data['trace'] = $this->trace;
        }
        Log::warning($this->status_message, [
            'trace' => $this->trace,
            'reasons' => $reasons,
        ]);
        return new Response($data, $this->status_code);
    }
}
