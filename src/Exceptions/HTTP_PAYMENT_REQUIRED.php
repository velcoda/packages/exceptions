<?php

namespace Velcoda\Exceptions\Exceptions;

class HTTP_PAYMENT_REQUIRED extends Base // phpcs:disable Squiz.Classes.ValidClassName
{
    protected $status_code = 402;
    protected $status_message = 'HTTP_PAYMENT_REQUIRED';
}
