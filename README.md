# exceptions

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)
[![Travis](https://img.shields.io/travis/velcoda/exceptions.svg?style=flat-square)]()
[![Total Downloads](https://img.shields.io/packagist/dt/velcoda/exceptions.svg?style=flat-square)](https://packagist.org/packages/velcoda/exceptions)


## Install

```bash
composer require velcoda/exceptions
```


## Usage

Write a few lines about the usage of this package.


## Testing

Run the tests with:

```bash
vendor/bin/phpunit
```


## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.


## Security

If you discover any security-related issues, please email tech@velcoda.com instead of using the issue tracker.


## License

The MIT License (MIT). Please see [License File](/LICENSE.md) for more information.